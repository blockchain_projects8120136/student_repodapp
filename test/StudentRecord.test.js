//Import the StudentList smart contract
const StudentRecord = artifacts.require('StudentRecord')

//Use the contract to write all tests
//variable: account => all accounts in blockchain
contract('StudentRecord', (accounts) => {
    //Make sure contract is deployed and before
    //we retrieve the studentrecord object for testing
    beforeEach(async () => {
        this.studentRecord = await StudentRecord.deployed()
    })

    //Testing the deployed student contract
    it('Deployed successfully', async () => {
        //Get the address which the student object is stored
        const address = await this.studentRecord.address
        //Test for valid address
        isValidAddress(address)
    })
    //Testing the content in the contract
    it('Added the students successfully', async () => {
        return StudentRecord.deployed().then((instance) => {
            studentrecordInstance = instance;
            studentNo = 12210009;
            return studentrecordInstance.addStudent(studentNo, "Jigme Namgyel");

        }).then((transaction) => {
            isValidAddress(transaction.tx)
            isValidAddress(transaction.receipt.blockHash);
            return studentrecordInstance.studentsCount()
        })
        // .then((count) => {
        //     assert.equal(count, 1)
        //     return studentrecordInstance.students(1);
        // }).then((student) => {
        //     assert.equal(student.sid, studentNo)
        // })
    })

    // find the student
    it('Successful search of student', async () => {
        return StudentRecord.deployed().then(async (instance) => {
          s = instance;
          studentid = 2;
          return s.addStudent(studentid++, "Pema Yangzom").then(async (tx) => {
            return s.addStudent(studentid++, "Rigden Yoesel").then(async (tx) => {
              return s.addStudent(studentid++, "Sonam Tshering").then(async (tx) => {
                return s.addStudent(studentid++, "Tshering Dorji").then(async (tx) => {
                  return s.addStudent(studentid++, "Bijay Kumar Rai").then(async (tx) => {
                    return s.addStudent(studentid++, "Chencho Dema").then(async (tx) => {
                      return s.studentsCount().then(async (count) => {
                        assert.equal(count, 8)
                        return s.findStudent(5).then(async (student) => {
                          assert.equal(student.name, "Sonam Tshering")
                        })
                      })
                    })
                  })
                })
              })
            })
          })
        })
      }) 
      
      // Mark graduation of student
      it('Successfully marked graduate students', async () => {
        return StudentRecord.deployed().then(async (instance) => {
            s = instance;
            return s.findStudent(1).then(async (ostudent) => {
                assert.equal(ostudent.name, "Choki Lhamo")
                assert.equal(ostudent.graduated, false)
                return s.markGraduated(1).then(async (transaction) => {
                    return s.findStudent(1).then(async (nstudent) => {
                        assert.equal(nstudent.name, "Choki Lhamo")
                        assert.equal(nstudent.graduated, true)
                        return
                    })
                })
            })
        })
    })
      
})

//This function check if the address is valid
function isValidAddress(address){
    assert.notEqual(address, 0x0)
    assert.notEqual(address, '')
    assert.notEqual(address, null)
    assert.notEqual(address, undefined)
}

